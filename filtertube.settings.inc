<?php
/**
 * @file
 * Administration settings form.
 */

/**
 * @hook_settings()
 *
 * Callback for path 'admin/config/media/filtertube'
 */
function filtertube_admin_settings() {
  global $base_path;
  $mpath = drupal_get_path('module', 'filtertube');
  drupal_add_css($mpath . '/filtertube-settings.css', array('type' => 'file', 'media' => 'all', 'group' => 'CSS_MODULE') );
  $form = array();

  _filtertube_fitvids_check();

  if (user_access('administer filters')) {
   $form['config'] = array(
    '#markup' => l(t('Configure Text Formats'), 'admin/config/content/formats'),
    );
  }

  $form['filtertube-options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Player Features'),
  );
    $preset = array();
    $preset['type'] = 'ul';
    $preset['attributes'] = array('class' => 'filtertube-preset-list');
    $preset['items'][] = t('Default &mdash; Standard YouTube Player with Control Bar.');
    $preset['items'][] = t('Autohide &mdash; Scrub & control bars moves below player while playing, until  Site Visitor hovers over the player.');
    $preset['items'][] = t("Chromeless &mdash; Player only shows 'play icon' over still frame.");
    $presets = theme('item_list',  $preset);
  $form['filtertube-options']['filtertube_base_player'] = array(
    '#type' => 'select',
    '#field_prefix' => t('Use') . ' ',
    '#field_suffix' => t('player.'),
    '#default_value' => variable_get('filtertube_base_player', 0),
    '#options' => array(
      0 => t('default'),
      1 => t('autohide'),
      2 => t('chromeless'),
    ),
    '#description' => $presets,
  );
  $form['filtertube-options']['filtertube_base_cleanup'] = array(
    '#type' => 'checkbox',
    '#title' => t('Adapt height of player automatically.'),
    '#default_value' => variable_get('filtertube_base_cleanup', 1),
    '#description' => t('If Autohide or Chromeless are selected the 25px of height need for the default palyer are subtracted.  This will help cleanup black-bars appearing on the top and bottom of the playback.  Of course, should the uploaded video source not be cleanly cropped results may vary.'),
 );

  $form['filtertube-setting'] = array(
    '#title' => t('YouTube Player Theme'),
    '#type' => 'fieldset',
  );
  $form['filtertube-setting']['image'] = array(
    '#markup' => theme('image', array( 'path' => $mpath . '/img/filtertube-player_bar.jpg', 'attributes' => array( 'border' => '0', 'align' => 'right', 'class' => array('filtertube-on-right') ) )  )
  );
  $form['filtertube-setting']['filtertube_base_theme'] = array(
    '#type' => 'radios',
    '#title' => t('Base Control Bar'),
    '#options' => array(0 => t('Dark'), 1 => t('Light')),
    '#default_value' => variable_get('filtertube_base_theme', 0),
  );
  $form['filtertube-setting']['filtertube_base_color'] = array(
    '#type' => 'radios',
    '#title' => t('Scrub Bar') . ' ',
    '#options' => array(
      0 => t('Red'),
      1 => t('White')
    ),
     '#default_value' => variable_get('filtertube_base_color', 0),
  );


  $form['filtertube-rel'] = array(
       '#title' => t('Desktop Compatibility*'),
   '#type' => 'fieldset',
  );
  $form['filtertube-rel']['filtertube_base_rel'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide Related Content'),
    '#default_value' => variable_get('filtertube_base_rel', 1),
    '#description' => t("Does not show 'next suggested video' after playing video"),
  );


  $form['filtertube-rel']['filtertube_base_info'] = array(
    '#type' => 'checkbox',
    '#title' => t('Showinfo'),
    '#default_value' => variable_get('filtertube_base_info', 0),
    '#description' => t('Display information like the video title and uploader before the video starts playing'),
  );

  $form['filtertube-rel']['filtertube_base_branding'] = array(
    '#type' => 'checkbox',
    '#title' => t('Modestbranding'),
    '#default_value' => variable_get('filtertube_base_branding', 1),
  );
  $form['filtertube-rel']['notes'] = array(
    '#markup' => t('*Youtube supports these abilities when their player detects if users web browser has the Flash&trade; plugin accessible.')
  );


  $form['filtertube-js'] = array(
       '#title' => t('YouTube Javascript API'),
   '#type' => 'fieldset',
  );
  $form['filtertube-js']['filtertube_base_js'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable JS API'),
    '#default_value' => variable_get('filtertube_base_js', 0),
    '#description' => t("The API is not actually used by the filtertube module.  The option is here to allow scripting to be added by another developer."),
  );
  $form['filtertube-js']['filtertube_base_origin'] = array(
    '#type' => 'textfield',
    '#title' => t('Origin'),
    '#size' => 18,
    '#maxlength' => 255,
    '#default_value' => variable_get('filtertube_base_origin', $_SERVER['SERVER_NAME']),
  );

 $form['footnote']['notes'] = array(
    '#markup' => t('These settings are applied to ALL Text Formats with an active filtertube filter.')
  );





  $form['#submit'][] = 'filtertube_admin_settings_submit';
  if (!isset($form['#theme'])) {
    $form['#theme'] = 'filtertube_admin_settings';
  }
return system_settings_form($form);
}

/**
 * @hook_settings_submit()
 *
 */
function filtertube_admin_settings_submit($form_id, $form_state) {
  form_state_values_clean($form_state);

  foreach ($form_state['values'] as $key => $value) {
    if (is_array($value) && isset($form_state['values']['array_filter'])) {
      $value = array_keys(array_filter($value));
    }
    variable_set($key, $value);
  }
  filtertube_rebuild_cache();
}