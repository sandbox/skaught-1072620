;(function( $ ){
  'use strict';
  $.fn.pulseMessage = function(element) {
    this.fadeIn('50').animate({opacity: '1'}, 500, function(){
       $(this).animate({opacity: '0'}, 250,function(){
          $(this).animate({opacity: '1'}, 500);
       });
    });
   };
})( jQuery );
