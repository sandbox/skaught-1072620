;(function ($) {
	'use strict';
	Drupal.behaviors.filtertubeFitVids = {
	attach: function (context) {
		jQuery(".filtertube-embed-wrap.filtertube-fitvids").fitVids();
	}
};
}(jQuery));
