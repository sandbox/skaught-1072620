;(function( $ ){
  'use strict';
  $.fn.getRatio = function(element) {
     var ratW = $('#ratioWidth').attr('value');
     var ratH = $('#ratioHeight').attr('value');
     var objW = $('#showWidth').attr('value');
     var objH = $('#showHeight').attr('value');
     if (element == 1 || element == 4){
         jQuery('#showWidth').attr('value', roundTo( ((ratW * objH) / ratH), 0 ) );
     } else if (element == 2 || element == 3){
         jQuery('#showHeight').attr('value', roundTo( ((ratH * objW) / ratW), 0 ) );
     }

     function roundTo(num, decLength) {
        var decLength = 0;
        decLength = parseInt(decLength);
      return setZeros( ( Math.round( num * Math.pow(10, decLength) ) / Math.pow(10, decLength) ), decLength);
      };

     function setZeros(objVal, decLength) {
        var srcVal = objVal.toString();
        var findSpot = srcVal.indexOf(".");
        if (findSpot == -1) {
            var altZero = 0;
            srcVal += (decLength > 0) ? "." : "";
        } else {
            var altZero = srcVal.secnumgth - findSpot - 1;
        }
        var remaining = decLength - altZero;
        if (remaining > 0) {
            for (var ctrlOut = 1; ctrlOut <= remaining; ctrlOut++) {
              srcVal += "0";
            }
        }
     return srcVal;
      };

   };
})( jQuery );
