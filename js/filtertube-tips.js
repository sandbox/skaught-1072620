;(function ($) {
  'use strict';
  Drupal.behaviors.filtertube = {
  attach: function (context, settings) {

          var helpImg = jQuery('.filtertube-help-node');
          if (helpImg.length) {  // if target element(s) exists, continue...
            helpImg.prepend(Drupal.settings.filtertube.gif);
            jQuery('.filtertube-tip-behaviour').html(Drupal.settings.filtertube.tip);

            jQuery('.filtertube-tip-behaviour').click(function(event) {
               event.preventDefault();

               jQuery('#'+this.name).stop('FALSE', 'TRUE').slideToggle('600',function(){
                    var src= $(this).attr('id');
                    $(this).toggleClass('active');
                    if ( $(this).hasClass('active')) {
                       $('a[name="'+src+'"]').html(Drupal.settings.filtertube.tipless);
                    } else {
                      $('a[name="'+src+'"]').html(Drupal.settings.filtertube.tip);
                    }
               });
            });
          }

         jQuery('.hide-opts').hide();
         jQuery('.filtertube-opt-behaviour').html(Drupal.settings.filtertube.opt);
            jQuery('.filtertube-opt-behaviour').click(function(event) {
               event.preventDefault();
               jQuery('#'+this.name).stop('FALSE', 'TRUE').slideToggle('600',function(){
                    var src= $(this).attr('id');
                    $(this).toggleClass('active');
                    if ( $(this).hasClass('active')) {
                       $('a[name="'+src+'"]').html(Drupal.settings.filtertube.optless);
                    } else {
                      $('a[name="'+src+'"]').html(Drupal.settings.filtertube.opt);
                    }
               });

            });
    }
};
}(jQuery));
