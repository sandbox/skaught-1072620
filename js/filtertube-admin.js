;(function ($) {
  'use strict';
  Drupal.behaviors.filtertubeAdmin = {
  attach: function (context, settings) {

          var selected = jQuery('#edit-filters-filtertube-settings select option:selected');
   
          if (selected.attr('value')!='5' || selected.attr('value')!='-1') {
            jQuery('#filtertube-custom').addClass('collapsed');
          }

           var baseMsg = Drupal.t("Make sure to 'Save configuration'");
           $('#filtertube-message').html(baseMsg);

          jQuery('#edit-filters-filtertube-settings select').change(function() {
            var selected = jQuery('#edit-filters-filtertube-settings select option:selected');
            var currentState = $('#filtertube-custom').hasClass('collapsed');
            if (  (currentState) && (selected.attr('value')=='5' || selected.attr('value')=='-1')  ) {
                 Drupal.toggleFieldset('#filtertube-custom');
                 jQuery('#filtertube-message').pulseMessage();
            }
          });


        //  init ratio-widget

        // add enhanced form to page
          jQuery('#filtertube-ratio-widget .fieldset-description').before('<form id="filtertube-ratio-form"> \
               <small>'+Drupal.t('For best results, set the <u>width</u> of the video.')+'</small> \
              <input type="reset" id="filtertube-widget-reset" title="'+Drupal.t("Reset's Ratio Widget only.")+'" value=" '+Drupal.t('Reset Widget')+' " /> \
             <fieldset><p>'+Drupal.t("Aspect Ratio Setting")+'</p>\
              <label for="1" class="filtertube-aspect-label">Aspect Width</label><input type="number" min="1" max="100" maxlength="3" size="3" id="ratioWidth" class="num" value="16" name="1" /> : <label for="2" class="filtertube-aspect-label">Aspect Height</label><input type="number" min="1" max="100" maxlength="3" size="3" id="ratioHeight" class="num" value="9" name="2" /> \
              </fieldset> \
              <fieldset> \
              <p>'+Drupal.t('Desired Video Size')+'</p>\
              <label for="3" class="filtertube-aspect-label">Video <u>Width</u></label><input type="number" min="2" max="2800" maxlength="5" size="9" id="showWidth" class="num" value="320" name="3" /> &times; <label for="4" class="filtertube-aspect-label">Video Height</label><input type="number" min="1" max="1575" maxlength="5" size="9" id="showHeight" class="num" value="180" name="4" /> \
              </fieldset> \
              <fieldset><input type="button" id="filtertube-widget-setter" value=" '+Drupal.t('Send to Custom Fields*')+' " /> <small id="filtertube-setter-note">'+Drupal.t('*Will also add Player Bar height (25px)')+'</small> \
              </fieldset> \
              </form> \
              </fieldset>');
          jQuery('#filtertube-ratio-widget legend .fieldset-title').text('Ratio Helper Tool');

           // trigger math
           jQuery('#ratioWidth, #ratioHeight, #showWidth, #showHeight').bind('keyup change click', function() {
             jQuery(this).getRatio( jQuery(this).attr('name') );
           });

           // allow int only
           jQuery('.num').keydown(function(event) {
              // this trigger ket
              var thisKey = event.which;
              //arrowkeys | base numbers | numpad | forward-del & delete
              var allowedKeys=new Array(37,39, 48,49,50,51,52,53,54,55,56,57, 96,97,98,99,100,101,102,103,104,105, 8,46);
              //  esc, enter/return & command keys
              var breakOutKeys=new Array(27,13,18,18,91);
              var tabKey=9;

              if (  jQuery.inArray(thisKey, allowedKeys)>=0 ) {
               // (num/numPad) or (backspace,arrow left/right,forward del)
                return true;
              } else if ( jQuery.inArray(thisKey, breakOutKeys)>=0 ) {
                 // change focus. avoid's 'frozen keyboard' effect.
                jQuery(this).blur();
                jQuery('#filtertube-widget-setter').focus();
              } else if (thisKey==tabKey) {
                 // tab focus
                 var self = jQuery(this).attr('id') ;
                 var next = jQuery('#'+self).next();
                 next.focus();
              } else {
                return false;
              }

           });

           // init - help fix tab focus on both scriped & non-scritted 'num' elements
           jQuery('.num').each(function(index) {
              var count = index;
              var id= jQuery(this).attr('id')
             // alert(index + ': ' + $(this).attr('id'));
           });


           jQuery('#filtertube-widget-setter').click(function() {
              var setWidth = jQuery('#showWidth').attr('value');
              var presetHeight = parseInt(jQuery('#showHeight').attr('value'));
              var setHeight= presetHeight+25;
              jQuery('#edit-filters-filtertube-settings-tube-custom-opt-size-five-width').attr({'value':setWidth});
              jQuery('#edit-filters-filtertube-settings-tube-custom-opt-size-five-height').attr({'value':setHeight});
              jQuery('#filtertube-message').pulseMessage();
              jQuery('.filtertube-post-set').addClass('collapsed');

            });


    }
};
}(jQuery));
