<?php

/**
 * @file
 * drupal's hook_help() and hook_filter_tips().
 * contains most of the English text required for communication
 *
 */


/**
 * Implementation of hook_filter_tips()
 */
function filtertube_filter_tips($filter, $format, $long) {
  $override = $filter->settings['override'];
  $custom_size = $filter->settings['default'];
  $opt_size=array();
  $opt1= array(
     'attributes' => array(
      'class' => array('filtertube-tip-behaviour'),
      'name' => 'filter-tip-' . $filter->format
     ),
     'fragment' => 'filter-tip-' . $filter->format,
     'external' => TRUE,
   );
     $opt2= array(
     'attributes' => array(
      'class' => array('filtertube-opt-behaviour'),
      'name' => 'filter-opt-' . $filter->format
     ),
     'fragment' => 'filter-opt-' . $filter->format,
     'external' => TRUE,
   );
   $mini_help = l(t(''), url( current_path() ), $opt1); // title blank for tip-behaviour
   $mini_js_help = l(t(''), url( current_path() ), $opt2); // title blank for tip-behaviour
   $admin_filters='';
    if (user_access('administer filters')) {
      $admin_filters .= l(t('CONFIGURE') . ' [' . $filter->format . ']', 'admin/config/content/formats/' . $filter->format, array('html' => TRUE, 'attributes' => array('class' => array('filtertube-config')) )  );
   }
   $admin_help = '';
    if (user_access('access administration pages')) {
    $admin_help .= l(t('ADMIN HELP'), 'admin/help/filtertube');
   }
   $output  = '';
   $output .= '<dl class="filtertube-tip"><dt>';
   $output .= t('filtertube');
   $output .= '<span class="filtertube-config">&nbsp;&nbsp;&nbsp;' . $mini_js_help . '&nbsp;&nbsp;&nbsp;' . $mini_help . '&nbsp;&nbsp;&nbsp;' . $admin_help . '</span>';
   $output .= '</dt><dd>';
   $output .=  _filtertube_wysiwyg_helper_alt($filter->settings['override']) . '<br />';
   $output .= _filtertube_basic_eng() . '  ' . _filtertube_yid_helper_alt();
   if (!$long) {
     $output .=  '  ' . _filtertube_htmlNote_eng();
   }
   $output .= '<div class="filtertube-help-node" id="filter-tip-' . $filter->format . '"></div>';
  // if ($override=='1') {
     $output .= '<div class="clearfix"></div>';
     $output .= '<div class="hide-opts" id="filter-opt-' . $filter->format . '"><hr />' . _filtertube_optText_eng($filter) . '</div>';
 //  }
   if ($long) {
    $output .= '<div class="clearfix"></div><hr />' . _filtertube_trouble_eng();
   }
   $output .= '<div class="clearfix"></div><hr />';
   $opt_size=_filtertube_sizeList($filter);
   if ($override!='1') {
    $output .= t('Default Size:') . '&nbsp;&nbsp;' . $opt_size[$custom_size];
   } else {
    $output .= t('Embed Size:') . '&nbsp;&nbsp;<span class="hide-key">' . $opt_size[$custom_size] . '</span>';
   }
   $output .= '&nbsp;&nbsp;&nbsp;' . $admin_filters;
   $output .= '</dd>';
   $output .= '</dl>';
return $output;
}



/**
 * Implementation of hook_help()
 */
function filtertube_help($path, $arg) {
  global $user;
  global $base_path;
  $header = '';
  $body = '';
  $ext_tip = '';
  $footer = '';
  $output = '';
  $filter = new stdClass;
  $filter->settings['override'] = '0';
  $settings_link = '';
  if (user_access('administer filters')) {
    $settings_link = l(t('Configure Text Formats'), 'admin/config/content/formats');
  }

  $header .= '<p>' . t("This Module is an Text Filter allowing Users to easily add YouTube videos into their content. Using YouTube's iFrame embed method for h.246 video compatible playback, thus ensuring playback across a variety of more current devices (mobile webkit/android) in addition to those old school browsers.") . '</p>';
  $ext_tip .= '<blockquote><h2>' . t('Filter Syntax:') . '</h2>';
  $ext_tip .= '<p>' . _filtertube_wysiwyg_helper_alt() . '<br />';
  $ext_tip .= '<br />' . _filtertube_wysiwyg_helper($filter);
  $ext_tip .= '<br />';
  $ext_tip .= _filtertube_basic_eng() . _filtertube_yid_helper_alt() . '<br />';
  $ext_tip .= _filtertube_yid_helper() . '</p>';
  $ext_tip .=  '<p>' . _filtertube_optText_eng($filter) . '</p><div class="clearfix"></div><hr />';
  $ext_tip .= '</blockquote>';

   $footer .= _filtertube_trouble_eng() . '<br /><hr /><br />' . $settings_link ;
  switch ($path) {
   case 'admin/help#filtertube':
      $output .= '<div id="filtertube-help">' . "\n" . $header . "\n" . $ext_tip . "\n" . "\n" . $footer . "\n" . '</div>';
   break;
  }
return $output;
}


/*
 * renders a list of all size options available to 'this' filter
 */
function _filtertube_sizeList($filter) {
  (is_object($filter)) ? $dngk = TRUE : $filter = new stdClass; $filter->format = 'default';$filter->settings['height'] = '170'; $filter->settings['width'] = '280';
  $height = ( isset($filter->settings['tube']['custom']['opt_size_five']['height']) ) ? $filter->settings['tube']['custom']['opt_size_five']['height'] : $filter->settings['height'];
  $width = ( isset($filter->settings['tube']['custom']['opt_size_five']['width']) ) ? $filter->settings['tube']['custom']['opt_size_five']['width'] : $filter->settings['width'];
  $opt_size=array();
  $opt_width = _filtertube_opt_width();
  $opt_height = _filtertube_opt_height();
  $cleanup = variable_get('filtertube_base_cleanup', 1);
  $player = variable_get('filtertube_base_player', 0);
  if ( ($player > 0) && ($cleanup == "1") ) {
    $height = $height-25;
  }
  $opt_size['-1'] = FILTERTUBE_RESPONSE_NAME_CASE. ' ' . t('Responsive'); /* css choice */

  foreach ($opt_height as $key => $value) {
      $presets = $opt_height[$key];
     if ( ($player > 0) && ($cleanup == "1") ) {
      $presets = $opt_height[$key]-25;
    }
    $opt_size[$key] = '<span class="filter-size-key">' . $key . '.</span>&nbsp;<span class="filter-strong"> ' . $opt_width[$key] . '&times;' . $presets . '</span>';
  }
  $opt_size['5'] = '<span class="filter-size-key">5.</span>&nbsp;<span class="filter-strong">' . $width . '&times;' . $height . '</span>'; /* css choice */
return $opt_size;
}



/**
 *  image alt text for gif animation
 */
function _filtertube_basic_eng() {
   return t("Of course, You may enter the complete YouTube URL or simply") . ' '; /* has whiteSpace */
}


/**
 *  image alt text for gif animation
 */
function _filtertube_yid_helper_alt() {
   return t("enter the YouTube video 'ID' which lies between the text of (?v=) and (&) in your Browsers URL bar.");
}

/**
 *  image alt text for jpeg image
 *  note: $starter is not translatable.
 */
function _filtertube_wysiwyg_helper_alt($override=0) {
  $output = '';
  $intro = t('Enter');
  $starter = '[youtube:';
  $url = t('ID/URL');
  $opt = t('OPTIONS');
  $close = '] ';

  $output .= $intro . ' ' . $starter . $url;
  //if ($override == 0) {
  $output .=  ':' . $opt;
  //}
  $output .= $close;

  $output .= t('into the area of text you wish the video to be placed in.');
return $output;
}

/**
 *  user options 'size' description for hook_tips
 */
function _filtertube_sizeTxt_eng() {
   return  t("'size' &mdash;  As a numerical value.  Example:  [youtube:DX1iplQQJTo:<b>2</b>]");
}

/**
 *  user help text
 */
function  _filtertube_htmlNote_eng() {
  return '<b>' . t('DO NOT ENTER ANY HTML') . '</b>';
}

/**
 *  User options text descriptions for hook_tips
 *
 * t() note:  option triggers are not translatable, they are
 *      param based command calls, not language strings.
 *
 * todo: cleanup html/css to list element.
 */
function _filtertube_optText_eng($filter) {
  $override = $filter->settings['override'];
  $output = array();
  $output['title'] = 'OPTIONS (separated by spaces, order does not matter)';
  $output['type'] = 'ul';
  $output['attributes'] = array('class' => 'filtertube-option-list');
  $output['items'][] = "start(0000) &mdash; " . t('Time to start playback at.  State in seconds, inserted into brackets.') . '<br /><small>' . t('Only works with primary ID') . '</small>';
  $output['items'][] = "playlist|xxx|yyy|zzz &mdash; " . t("to create, make a 'pipe (|) separated list' of other YouTube ID's.") . '<br /><small>' . t("Must be just ID's, no URL's.") . '</small>';
  $output['items'][] = "loop &mdash; " . t('Loops playback.');
   if ($override==0) {
  $output['items'][] = "play/autoplay &mdash; " . t('Begin video playback on page load.');
  $opt_size = _filtertube_sizeList($filter);
  $output['items'][] = theme_item_list(array('items' => $opt_size, 'title' => _filtertube_sizeTxt_eng(), 'type' => 'ul', 'attributes' => array('class' => 'filtertube-size-list')) ) . '<em>' . t('User sizes choices & autoplay with care') . '</em>';
  }

return theme('item_list',  $output);
}

/**
 *  hook_help trouble-shooting text
 */
function _filtertube_trouble_eng() {
  $trouble = array();
  $trouble['title'] = 'Troubleshooting';
  $trouble['type'] = 'ul';
  $trouble['attributes'] = array('class' => 'filtertube-trouble-list');
  $trouble['items'][] =  _filtertube_htmlNote_eng() . '&hellip;' . t('it will not be processed through this module. That end result will depend on what other Filters have been applied to this configuation.');
    $introuble = array();
    $introuble['type'] = 'ul';
    $introuble['title'] = 'Possibly&hellip;';
    $introuble['attributes'] = array('class' => array('filtertube-inset-list') );
    $introuble['items'][] = t('YouTube has not yet renedered this video format and is now in the process of generateing it!  Try reloading the page after a few minutes.');
    $introuble['items'][] = t('The YouTube ID/URL entered does not exist!  Please, check your input data.') . '</li>';
    $introuble['items'][] = t('YouTube maybe experiencing difficulties with their own serves.  Third-party services are sometimes the trouble, buy their own nature.');
  $trouble['items'][] =  t('An iframe space appears, but neither an image placholder nor player controls follow&hellip;' . theme('item_list',  $introuble));
  $opt= array('html' => TRUE, 'fragment' => ' ', 'external' => TRUE);
  $trouble['items'][] = t("Just a link (like \"!link\") appears in the page's content:  Your Text Format is filtering &lt;!iframe&gt; & &lt;!div&gt; tags", array('!link' => l(t('youtube.com|DX1iplQQJTo &raquo;'), '', $opt), '!iframe'=>'iframe', '!div'=>'div' ) );
return theme('item_list',  $trouble);
}